
package org.danny.backend;

/**
 * 31/01/2017
 * @author danny_suarez based on  C Language Solution https://github.com/fjsj/programming-challenges/blob/master/LCD%20Display.c
 */
public class DisplayNumberTest {



	private int nLines;
	private int nColumns;
	private String[][] lcdNumber=new String[23][12];
	private String[][][] lcdNumbers=new String[8][23][12];
	
	/**
	 * Metodo principal
	 * Por favor para hacer pruebas basta con Llenar el StringBuilder de acuerdo 
	 * @param args
	 */
	public static void main(String[] args) {
		StringBuilder outputText=new StringBuilder();
		StringBuilder inputText=new StringBuilder("4 32"+"\n"); 
		inputText.append("2 27");//TODO De esta forma usted puede adicionar los valores que quiera
		inputText.append("0 0");
		DisplayNumberTest exercise=new DisplayNumberTest();
		String strNumberText=inputText.toString();
		
		
		String[] separatedText=strNumberText.split("\\n");
		
		for(String txtTmp:separatedText){
			int size=Integer.parseInt(txtTmp.split(" ")[0]);
			String strNumber=txtTmp.split(" ")[1];
			if(size==0&&Integer.parseInt(strNumber)==0){
				break;
			}
//			while (size != 0 || Integer.parseInt(strNumber) != 0) {
				exercise.setLcdSize(size);
				int length = strNumber.length();
				
				for (int i = 0; i < length; i++) {
					int n = strNumber.charAt(i) - '0';
					exercise.printNumber(n, i);
				}

				for (int i = 0; i < exercise.getnLines(); i++) {
					for (int n = 0; n < length; n++) {
						for (int j = 0; j < exercise.getnColumns(); j++) {
							outputText.append(exercise.getLcdNumbers()[n][i][j]);
						}
						if (n < length - 1) {
							outputText.append(" ");; //column of blanks
						}
					}
					outputText.append("\n");
				}
				outputText.append("\n");
//			}
		}
		System.out.println(outputText.toString());
		
	}
	

	public int getnLines() {
		return nLines;
	}

	public void setnLines(int nLines) {
		this.nLines = nLines;
	}

	public int getnColumns() {
		return nColumns;
	}

	public void setnColumns(int nColumns) {
		this.nColumns = nColumns;
	}

	public String[][] getLcdNumber() {
		return lcdNumber;
	}

	public void setLcdNumber(String[][] lcdNumber) {
		this.lcdNumber = lcdNumber;
	}

	public String[][][] getLcdNumbers() {
		return lcdNumbers;
	}

	public void setLcdNumbers(String[][][] lcdNumbers) {
		this.lcdNumbers = lcdNumbers;
	}

	public void setLcdSize(int size) {
		nLines = 2 * size + 3;
		nColumns = size + 2;
	}

	public void clearLcd() {
		int i;
		for (i = 0; i < nLines; i++) {
			int j;
			for (j = 0; j < nColumns; j++) {
				lcdNumber[i][j] = " ";
			}
		}    
	}

	public void drawLine(int lineNumber) {
		int j;
		for (j = 1; j < nColumns - 1; j++) {
			lcdNumber[lineNumber][j] = "-";
		}
	}

	public void drawFirstLine() {
		drawLine(0);
	}

	public void drawMiddleLine() {
		drawLine(nLines / 2);
	}

	public void drawLastLine() {
		drawLine(nLines - 1);
	}

	public void drawFirstColumn(int columnNumber) {
		int i;
		for (i = 1; i < nLines / 2; i++) {
			lcdNumber[i][columnNumber] = "|";    
		}
	}

	public void drawLastColumn(int columnNumber) {
		int i;
		for (i = (nLines / 2) + 1; i < nLines - 1; i++) {
			lcdNumber[i][columnNumber] = "|";    
		}
	}

	public void drawFirstLeftColumn() {
		drawFirstColumn(0);
	}

	public void drawLastLeftColumn() {
		drawLastColumn(0);
	}

	public void drawFirstRightColumn() {
		drawFirstColumn(nColumns - 1);
	}

	public void drawLastRightColumn() {
		drawLastColumn(nColumns - 1);
	}

	public void printLcd(int position) {
		int i;
		for (i = 0; i < nLines; i++) {
			int j;
			for (j = 0; j < nColumns; j++) {
				lcdNumbers[position][i][j] = lcdNumber[i][j];
			}
		}    
	}

	public void printZero(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstLeftColumn();
		drawFirstRightColumn();
		drawLastLeftColumn();
		drawLastRightColumn();
		drawLastLine();
		printLcd(position);
	}

	public void printOne(int position) {
		clearLcd();
		drawFirstRightColumn();
		drawLastRightColumn();
		printLcd(position);
	}

	public void printTwo(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstRightColumn();
		drawMiddleLine();
		drawLastLeftColumn();
		drawLastLine();
		printLcd(position);
	}

	public void printThree(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstRightColumn();
		drawMiddleLine();
		drawLastRightColumn();
		drawLastLine();
		printLcd(position);
	}

	public void printFour(int position) {
		clearLcd();
		drawFirstLeftColumn();
		drawFirstRightColumn();
		drawMiddleLine();
		drawLastRightColumn();
		printLcd(position);
	}

	public void printFive(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstLeftColumn();
		drawMiddleLine();
		drawLastRightColumn();
		drawLastLine();
		printLcd(position);
	}

	public void printSix(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstLeftColumn();
		drawMiddleLine();
		drawLastLeftColumn();
		drawLastRightColumn();
		drawLastLine();
		printLcd(position);
	}

	public void printSeven(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstRightColumn();
		drawLastRightColumn();
		printLcd(position);
	}

	public void printEight(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstLeftColumn();
		drawFirstRightColumn();
		drawMiddleLine();
		drawLastLeftColumn();
		drawLastRightColumn();
		drawLastLine();
		printLcd(position);
	}

	public void printNine(int position) {
		clearLcd();
		drawFirstLine();
		drawFirstLeftColumn();
		drawFirstRightColumn();
		drawMiddleLine();
		drawLastRightColumn();
		drawLastLine();
		printLcd(position);
	}



	public void printNumber(int n, int position) {
		if(n==0){
			printZero(position);
		}else if(n==1){
			printOne(position);
		}else if(n==2){
			printTwo(position);
		}else if(n==3){
			printThree(position);
		}else if(n==4){
			printFour(position);
		}else if(n==5){
			printFive(position);
		}else if(n==6){
			printSix(position);
		}else if(n==7){
			printSeven(position);
		}else if(n==8){
			printEight(position);
		}else if(n==9){
			printNine(position);

		}
	}
	
	




}