/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.danny.backend;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="dtBasicView") 
@ViewScoped
public class BasicView implements Serializable {
    

    @PostConstruct
    public void init() {
       
    }
	
	private String inputString;

	private String outputString;


	//Ejecutar el main para probar la solución
	public void processValue() {
		try{
			outputString=DisplayNumber.processString(inputString);
			
		}catch(Exception e){
			outputString="";
		}
		
	}





	public String getInputString() {
		return inputString;
	}





	public void setInputString(String inputString) {
		this.inputString = inputString;
	}





	public String getOutputString() {
		return outputString;
	}





	public void setOutputString(String outputString) {
		this.outputString = outputString;
	}

	
	

    
}
